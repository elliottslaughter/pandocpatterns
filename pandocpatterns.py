# Copyright (c) 2015, Elliott Slaughter <elliottslaughter@gmail.com>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#

from pandocfilters import walk


# TODO: Validate inputs (e.g. length must be int, etc.)
class Query:
    def __init__(self):
        self._checks = []

    #
    # Block Queries
    #

    def Pandoc(self, length=None):
        query = Query()
        query._init(self)
        query._check_isdoc()
        query._check_length_doc(length)
        return query

    def Plain(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Plain')
        query._check_length(None, length)
        return query

    def Para(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Para')
        query._check_length(None, length)
        return query

    def CodeBlock(self, hasID=None, hasClass=None, text=None):
        query = Query()
        query._init(self)
        query._check_type('CodeBlock')
        query._check_hasid(0, hasID)
        query._check_hasclass(0, hasClass)
        query._check_field(1, text)
        return query

    def RawBlock(self, hasFormat=None, text=None):
        query = Query()
        query._init(self)
        query._check_type('RawBlock')
        query._check_field(0, hasFormat)
        query._check_field(1, text)
        return query

    def BlockQuote(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('BlockQuote')
        query._check_length(None, length)
        return query

    # TODO: Add ListAttribute to OrderedList.
    def OrderedList(self, length=None, sublist_length=None):
        query = Query()
        query._init(self)
        query._check_type('OrderedList')
        query._check_length(1, length)
        query._check_sublist_length(1, sublist_length)
        return query

    def BulletList(self, length=None, sublist_length=None):
        query = Query()
        query._init(self)
        query._check_type('BulletList')
        query._check_length(None, length)
        query._check_sublist_length(None, sublist_length)
        return query

    # TODO: Add DefinitionList.

    def Header(self, level=None, hasID=None, hasClass=None, length=None):
        query = Query()
        query._init(self)
        query._check_type('Header')
        query._check_field(0, level)
        query._check_hasid(1, hasID)
        query._check_hasclass(1, hasClass)
        query._check_length(2, length)
        return query

    def HorizontalRule(self):
        query = Query()
        query._init(self)
        query._check_type('HorizontalRule')
        return query

    # TODO: Add Table.

    def Div(self, hasID=None, hasClass=None, length=None):
        query = Query()
        query._init(self)
        query._check_type('Div')
        query._check_hasid(0, hasID)
        query._check_hasclass(0, hasClass)
        query._check_length(1, length)
        return query

    def Null(self):
        query = Query()
        query._init(self)
        query._check_type('Null')
        return query

    #
    # Inline Queries
    #

    def Str(self, text=None):
        query = Query()
        query._init(self)
        query._check_type('Str')
        query._check_field(None, text)
        return query

    def Emph(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Emph')
        query._check_length(None, length)
        return query

    def Strong(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Strong')
        query._check_length(None, length)
        return query

    def Strikeout(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Strikeout')
        query._check_length(None, length)
        return query

    def Superscript(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Superscript')
        query._check_length(None, length)
        return query

    def Subscript(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Subscript')
        query._check_length(None, length)
        return query

    def SmallCaps(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('SmallCaps')
        query._check_length(None, length)
        return query

    # TODO: Add QuoteType for Quoted.
    def Quoted(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Quoted')
        query._check_length(1, length)
        return query

    # TODO: Add Citation for Cite.
    def Cite(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Cite')
        query._check_length(1, length)
        return query

    def Code(self, hasID=None, hasClass=None, text=None):
        query = Query()
        query._init(self)
        query._check_type('Code')
        query._check_hasid(0, hasID)
        query._check_hasclass(0, hasClass)
        query._check_field(1, text)
        return query

    def Space(self):
        query = Query()
        query._init(self)
        query._check_type('Space')
        return query

    def LineBreak(self):
        query = Query()
        query._init(self)
        query._check_type('LineBreak')
        return query

    def Math(self, text=None):
        query = Query()
        query._init(self)
        query._check_type('Math')
        query._check_field(1, text)
        return query

    def RawInline(self, hasFormat=None, text=None):
        query = Query()
        query._init(self)
        query._check_type('RawInline')
        query._check_field(0, hasFormat)
        query._check_field(1, text)
        return query

    # TODO: Add Target for Link.
    def Link(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Link')
        query._check_length(0, length)
        return query

    # TODO: Add Target for Image.
    def Image(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Image')
        query._check_length(0, length)
        return query

    def Note(self, length=None):
        query = Query()
        query._init(self)
        query._check_type('Note')
        query._check_length(None, length)
        return query

    def Span(self, hasID=None, hasClass=None, length=None):
        query = Query()
        query._init(self)
        query._check_type('Span')
        query._check_hasid(0, hasID)
        query._check_hasclass(0, hasClass)
        query._check_length(1, length)
        return query

    #
    # Child Queries
    #

    def __getitem__(self, key):
        query = Query()
        query._init(self)
        query._check_child(key)
        return query

    #
    # Execution
    #

    def match(self, element):
        match = Match(element)
        for check_fn, check_args in self._checks:
            if not check_fn(match, *check_args):
                return None
        return match

    def m(self, element):
        """Shorthand for query.match(element) is not None."""
        return self.match(element) is not None

    def search(self, element, count=0):
        results = []

        def search_element(element_):
            match = self.match(element_)
            if match is not None and (count == 0 or len(results) < count):
                results.append(match.group(0))

        def search_wrapper(key, value, _1, _2):
            element_ = {'t': key, 'c': value}
            search_element(element_)

        search_element(element)
        walk(element, search_wrapper, None, None)
        if len(results) == 0:
            return None
        return Match(results)

    def s(self, element, count=0):
        """Shorthand for query.search(element, count) is not None."""
        return self.search(element, count) is not None

    def replace(self, element, replacement_fn, count=0):
        assert False and "unimplimented"

    #
    # Internal Implementation
    #

    def _init(self, query):
        assert(len(self._checks) == 0)
        self._checks.extend(query._checks)

    def _check_isdoc(self):
        self._checks.append((Match._check_isdoc, ()))

    def _check_type(self, expected_type):
        if expected_type is not None:
            self._checks.append((Match._check_type, (expected_type,)))

    def _check_field(self, fid, expected):
        if expected is not None:
            self._checks.append((Match._check_field, (fid, expected)))

    def _check_length(self, fid, expected_length):
        if expected_length is not None:
            self._checks.append((Match._check_length, (fid, expected_length)))

    def _check_length_doc(self, expected_length):
        if expected_length is not None:
            self._checks.append((Match._check_length_doc, (expected_length,)))

    def _check_sublist_length(self, fid, expected_length):
        if expected_length is not None:
            self._checks.append((Match._check_sublist_length,
                                (fid, expected_length)))

    def _check_hasid(self, fid, expected_id):
        if expected_id is not None:
            self._checks.append((Match._check_hasid, (fid, expected_id)))

    def _check_hasclass(self, fid, expected_class):
        if expected_class is not None:
            self._checks.append((Match._check_hasclass, (fid, expected_class)))

    def _check_child(self, key):
        self._checks.append((Match._check_child, (key,)))


class Match:
    def __init__(self, value):
        self._value = value

    def group(self, index):
        if index == 0:
            return self._value
        else:
            assert False and "unimplemented"

    #
    # Internal Implementation
    #

    def _get_field(self, fid):
        if fid is None:
            return self._value['c']
        return self._value['c'][fid]

    def _check_isdoc(self):
        return (isinstance(self._value, list) and len(self._value) == 2 and
                isinstance(self._value[0], dict) and
                'unMeta' in self._value[0])

    def _check_type(self, expected_type):
        return (isinstance(self._value, dict) and 't' in self._value and
                self._value['t'] == expected_type)

    def _check_field(self, fid, expected):
        return self._get_field(fid) == expected

    def _check_length(self, fid, expected_length):
        length = len(self._get_field(fid))
        return (length == expected_length if expected_length >= 0 else
                length >= -expected_length)

    def _check_length_doc(self, expected_length):
        length = len(self._value[1])
        return (length == expected_length if expected_length >= 0 else
                length >= -expected_length)

    def _check_sublist_length(self, fid, expected_length):
        sublists = self._get_field(fid)
        for sublist in sublists:
            length = len(sublist)
            if ((expected_length >= 0 and length != expected_length) or
                    (expected_length < 0 and length < -expected_length)):
                return False
        return True

    def _check_hasid(self, fid, expected_id):
        return expected_id == self._get_field(fid)[0]

    def _check_hasclass(self, fid, expected_class):
        return expected_class in self._get_field(fid)[1]

    def _check_child(self, key):
        if isinstance(self._value, list):
            if (len(self._value) == 2 and isinstance(self._value[0], dict) and
                    'unMeta' in self._value[0]):
                children = self._value[1]
            else:
                children = self._value
        else:
            if self._value['t'] in ('Plain', 'Para', 'BlockQuote',
                                    'BulletList', 'Emph', 'Strong',
                                    'Strikeout', 'Superscript', 'Subscript',
                                    'SmallCaps'):
                fid = None
            elif self._value['t'] in ('Link', 'Image'):
                fid = 0
            elif self._value['t'] in ('OrderedList', 'Div', 'Quoted', 'Cite',
                                      'Span'):
                fid = 1
            elif self._value['t'] in ('Header'):
                fid = 2
            else:
                # Element does not contain children.
                return False
            children = self._get_field(fid)

        if key < len(children):
            self._value = children[key]
            return True
        assert False
