# pandocpatterns

A pattern matching library for querying Pandoc documents from Python.

**This library is a proof-of-concept.** It does not yet support the
full Pandoc AST, is not robust against failure, and the API is not
stable in any sense of the word. **Use at your own risk.**

# Usage

    import pandocpatterns

    # Suppose `block` is some Pandoc block element.
    block = ...

    # This query matches a BlockQuote containing a single Para
    # beginning with the word "Chapter" in small caps.
    q = Query().
            BlockQuote(length = 1)[0].
            Para()[0].
            SmallCaps(length = 1)[0].
            Str(text = 'Chapter')

    # The match() method returns a Match if the match was found, and
    # group(0) returns the matched node (in this case, the Str node at
    # the tail end of the query).
    m = q.match(block)
    if m is not None:
        x = m.group(0)
        ...

# License

Copyright (c) 2015, Elliott Slaughter <elliottslaughter@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
