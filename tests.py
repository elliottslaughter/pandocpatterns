# Copyright (c) 2015, Elliott Slaughter <elliottslaughter@gmail.com>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#

import json
import subprocess
import textwrap
import unittest

from pandocpatterns import Query as Q


class TestQuery(unittest.TestCase):
    def parse(self, markdown_text):
        pandoc = subprocess.Popen(
            ['pandoc', '-f', 'markdown', '-t', 'json', '--smart'],
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE)
        json_text, _ = pandoc.communicate(textwrap.dedent(markdown_text))
        assert pandoc.returncode == 0
        return json.loads(json_text)

    def test_match_pandoc(self):
        d = self.parse(r"""
        """)
        self.assertTrue(Q().Pandoc().m(d))
        self.assertTrue(Q().Pandoc(length=0).m(d))
        self.assertFalse(Q().Pandoc(length=1).m(d))
        self.assertFalse(Q().Pandoc(length=-1).m(d))

    def test_match_plain(self):
        d = self.parse(r"""
        * asdf
        """)
        self.assertTrue(Q().Pandoc()[0].BulletList()[0][0].Plain().m(d))
        self.assertTrue(
            Q().Pandoc()[0].BulletList()[0][0].Plain(length=1).m(d))
        self.assertTrue(
            Q().Pandoc()[0].BulletList()[0][0].Plain(length=-1).m(d))
        self.assertFalse(
            Q().Pandoc()[0].BulletList()[0][0].Plain(length=2).m(d))
        self.assertFalse(
            Q().Pandoc()[0].BulletList()[0][0].Plain(length=-2).m(d))

    def test_match_para(self):
        d = self.parse(r"""
        asdf
        """)
        self.assertTrue(Q().Pandoc()[0].Para().m(d))
        self.assertTrue(Q().Pandoc()[0].Para(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para(length=-2).m(d))

    def test_match_codeblock(self):
        d = self.parse(r"""
        ~~~ {#mycode .haskell .numberLines startFrom="100"}
        asdf
        ~~~
        """)
        self.assertTrue(Q().Pandoc()[0].CodeBlock().m(d))
        self.assertTrue(Q().Pandoc()[0].CodeBlock(hasID="mycode").m(d))
        self.assertFalse(Q().Pandoc()[0].CodeBlock(hasID="notme").m(d))
        self.assertTrue(Q().Pandoc()[0].CodeBlock(hasClass="haskell").m(d))
        self.assertFalse(Q().Pandoc()[0].CodeBlock(hasClass="notme").m(d))
        self.assertTrue(Q().Pandoc()[0].CodeBlock(text="asdf").m(d))
        self.assertFalse(Q().Pandoc()[0].CodeBlock(text="notme").m(d))

    def test_match_rawblock(self):
        d = self.parse(r"""
        \begin{something}
        asdf
        \end{something}
        """)
        self.assertTrue(Q().Pandoc()[0].RawBlock().m(d))
        self.assertTrue(Q().Pandoc()[0].RawBlock(hasFormat="latex").m(d))
        self.assertFalse(Q().Pandoc()[0].RawBlock(hasFormat="html").m(d))
        self.assertTrue(Q().Pandoc()[0].RawBlock(
            text='\\begin{something}\nasdf\n\\end{something}').m(d))
        self.assertFalse(Q().Pandoc()[0].RawBlock(text="notme").m(d))

    def test_match_blockquote(self):
        d = self.parse(r"""
        > asdf
        """)
        self.assertTrue(Q().Pandoc()[0].BlockQuote().m(d))
        self.assertTrue(Q().Pandoc()[0].BlockQuote(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].BlockQuote(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].BlockQuote(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].BlockQuote(length=-2).m(d))

    def test_match_orderedlist(self):
        d = self.parse(r"""
        1.  asdf
        2.  asdf

            asdf
        """)
        self.assertTrue(Q().Pandoc()[0].OrderedList().m(d))
        self.assertTrue(Q().Pandoc()[0].OrderedList(length=2).m(d))
        self.assertTrue(Q().Pandoc()[0].OrderedList(length=-1).m(d))
        self.assertTrue(Q().Pandoc()[0].OrderedList(length=-2).m(d))
        self.assertFalse(Q().Pandoc()[0].OrderedList(length=3).m(d))
        self.assertFalse(Q().Pandoc()[0].OrderedList(length=-3).m(d))
        self.assertTrue(Q().Pandoc()[0].OrderedList(sublist_length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].OrderedList(sublist_length=1).m(d))
        self.assertFalse(Q().Pandoc()[0].OrderedList(sublist_length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].OrderedList(sublist_length=-2).m(d))

    def test_match_bulletlist(self):
        d = self.parse(r"""
        *   asdf
        *   asdf

            asdf
        """)
        self.assertTrue(Q().Pandoc()[0].BulletList().m(d))
        self.assertTrue(Q().Pandoc()[0].BulletList(length=2).m(d))
        self.assertTrue(Q().Pandoc()[0].BulletList(length=-1).m(d))
        self.assertTrue(Q().Pandoc()[0].BulletList(length=-2).m(d))
        self.assertFalse(Q().Pandoc()[0].BulletList(length=3).m(d))
        self.assertFalse(Q().Pandoc()[0].BulletList(length=-3).m(d))
        self.assertTrue(Q().Pandoc()[0].BulletList(sublist_length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].BulletList(sublist_length=1).m(d))
        self.assertFalse(Q().Pandoc()[0].BulletList(sublist_length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].BulletList(sublist_length=-2).m(d))

    def test_match_header(self):
        d = self.parse(r"""
        ## asdf {#mycode .haskell .numberLines startFrom="100"}
        """)
        self.assertTrue(Q().Pandoc()[0].Header().m(d))
        self.assertTrue(Q().Pandoc()[0].Header(level=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Header(level=1).m(d))
        self.assertFalse(Q().Pandoc()[0].Header(level=3).m(d))
        self.assertTrue(Q().Pandoc()[0].Header(hasID="mycode").m(d))
        self.assertFalse(Q().Pandoc()[0].Header(hasID="notme").m(d))
        self.assertTrue(Q().Pandoc()[0].Header(hasClass="haskell").m(d))
        self.assertFalse(Q().Pandoc()[0].Header(hasClass="notme").m(d))
        self.assertTrue(Q().Pandoc()[0].Header(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Header(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Header(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Header(length=-2).m(d))

    def test_match_horizontalrule(self):
        d = self.parse(r"""
        ----
        """)
        self.assertTrue(Q().Pandoc()[0].HorizontalRule().m(d))

    def test_match_div(self):
        d = self.parse(r"""
        <div id="qwer" class="zxcv" uiop="hjkl">

        asdf

        </div>
        """)
        self.assertTrue(Q().Pandoc()[0].Div().m(d))
        self.assertTrue(Q().Pandoc()[0].Div(hasID="qwer").m(d))
        self.assertFalse(Q().Pandoc()[0].Div(hasID="notme").m(d))
        self.assertTrue(Q().Pandoc()[0].Div(hasClass="zxcv").m(d))
        self.assertFalse(Q().Pandoc()[0].Div(hasClass="notme").m(d))
        self.assertTrue(Q().Pandoc()[0].Div(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Div(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Div(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Div(length=-2).m(d))

    def test_match_str(self):
        d = self.parse(r"""
        asdf
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Str().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Str(text="asdf").m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Str(text="notme").m(d))

    def test_match_emph(self):
        d = self.parse(r"""
        *asdf*
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Emph().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Emph(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Emph(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Emph(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Emph(length=-2).m(d))

    def test_match_strong(self):
        d = self.parse(r"""
        **asdf**
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Strong().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Strong(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Strong(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Strong(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Strong(length=-2).m(d))

    def test_match_strikeout(self):
        d = self.parse(r"""
        ~~asdf~~
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Strikeout().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Strikeout(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Strikeout(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Strikeout(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Strikeout(length=-2).m(d))

    def test_match_superscript(self):
        d = self.parse(r"""
        ^asdf^
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Superscript().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Superscript(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Superscript(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Superscript(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Superscript(length=-2).m(d))

    def test_match_subscript(self):
        d = self.parse(r"""
        ~asdf~
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Subscript().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Subscript(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Subscript(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Subscript(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Subscript(length=-2).m(d))

    def test_match_smallcaps(self):
        d = self.parse(r"""
        <span style="font-variant:small-caps;">asdf</span>
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].SmallCaps().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].SmallCaps(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].SmallCaps(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].SmallCaps(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].SmallCaps(length=-2).m(d))

    def test_match_quoted(self):
        d = self.parse(r"""
        "asdf"
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Quoted().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Quoted(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Quoted(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Quoted(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Quoted(length=-2).m(d))

    def test_match_cite(self):
        d = self.parse(r"""
        [@asdf]
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Cite().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Cite(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Cite(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Cite(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Cite(length=-2).m(d))

    def test_match_code(self):
        d = self.parse(r"""
        `asdf`{#mycode .haskell .numberLines startFrom="100"}
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Code().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Code(hasID="mycode").m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Code(hasID="notme").m(d))
        self.assertTrue(
            Q().Pandoc()[0].Para()[0].Code(hasClass="haskell").m(d))
        self.assertFalse(
            Q().Pandoc()[0].Para()[0].Code(hasClass="notme").m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Code(text="asdf").m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Code(text="notme").m(d))

    def test_match_space(self):
        d = self.parse(r"""
        asdf asdf
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[1].Space().m(d))

    def test_match_linebreak(self):
        d = self.parse(r"""
        asdf \
        asdf
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[1].LineBreak().m(d))

    def test_match_math(self):
        d = self.parse(r"""
        $x$
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Math().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Math(text="x").m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Math(text="notme").m(d))

    def test_match_math(self):
        d = self.parse(r"""
        $x$
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Math().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Math(text="x").m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Math(text="notme").m(d))

    def test_match_rawinline(self):
        d = self.parse(r"""
        \textsf{asdf}
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].RawInline().m(d))
        self.assertTrue(
            Q().Pandoc()[0].Para()[0].RawInline(hasFormat="tex").m(d))
        self.assertFalse(
            Q().Pandoc()[0].Para()[0].RawInline(hasFormat="html").m(d))
        self.assertTrue(
            Q().Pandoc()[0].Para()[0].RawInline(text=r'\textsf{asdf}').m(d))
        self.assertFalse(
            Q().Pandoc()[0].Para()[0].RawInline(text="notme").m(d))

    def test_match_link(self):
        d = self.parse(r"""
        [asdf](http://asdf.com/)
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Link().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Link(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Link(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Link(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Link(length=-2).m(d))

    def test_match_image(self):
        d = self.parse(r"""
        ![asdf](http://asdf.com/)
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Image().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Image(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Image(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Image(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Image(length=-2).m(d))

    def test_match_note(self):
        d = self.parse(r"""
        asdf[^1]

        [^1]: qwer
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[1].Note().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[1].Note(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[1].Note(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[1].Note(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[1].Note(length=-2).m(d))

    def test_match_span(self):
        d = self.parse(r"""
        <span id="qwer" class="zxcv" uiop="hjkl">asdf</span>
        """)
        self.assertTrue(Q().Pandoc()[0].Para()[0].Span().m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Span(hasID="qwer").m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Span(hasID="notme").m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Span(hasClass="zxcv").m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Span(hasClass="notme").m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Span(length=1).m(d))
        self.assertTrue(Q().Pandoc()[0].Para()[0].Span(length=-1).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Span(length=2).m(d))
        self.assertFalse(Q().Pandoc()[0].Para()[0].Span(length=-2).m(d))

    def test_match_nested(self):
        d = self.parse(r"""
        *   **asdf**
        *   *uiop*

            > **_qwer_ zxcv**
        """)
        self.assertTrue(Q().Pandoc()[0].BulletList().m(d))
        self.assertTrue(Q().Pandoc()[0].BulletList()[1][1].BlockQuote().m(d))
        self.assertTrue(
            Q().Pandoc()[0].BulletList()[1][1].BlockQuote()[0].Para().m(d))
        self.assertTrue(
            Q().Pandoc()[0].BulletList()[1][1].BlockQuote()[0].Para()[0].
            Strong().m(d))
        self.assertTrue(
            Q().Pandoc()[0].BulletList()[1][1].BlockQuote()[0].Para()[0].
            Strong()[0].Emph().m(d))
        self.assertTrue(
            Q().Pandoc()[0].BulletList()[1][1].BlockQuote()[0].Para()[0].
            Strong()[0].Emph()[0].Str(text="qwer").m(d))

    def test_search_pandoc(self):
        d = self.parse(r"""
        """)
        self.assertTrue(Q().Pandoc().s(d))
        self.assertTrue(Q().Pandoc(length=0).s(d))
        self.assertFalse(Q().Pandoc(length=1).s(d))
        self.assertFalse(Q().Pandoc(length=-1).s(d))

    def test_search_plain(self):
        d = self.parse(r"""
        * asdf
        """)
        self.assertTrue(Q().Plain().s(d))
        self.assertTrue(Q().Plain(length=1).s(d))
        self.assertTrue(Q().Plain(length=-1).s(d))
        self.assertFalse(Q().Plain(length=2).s(d))
        self.assertFalse(Q().Plain(length=-2).s(d))

    def test_search_para(self):
        d = self.parse(r"""
        asdf
        """)
        self.assertTrue(Q().Para().s(d))
        self.assertTrue(Q().Para(length=1).s(d))
        self.assertTrue(Q().Para(length=-1).s(d))
        self.assertFalse(Q().Para(length=2).s(d))
        self.assertFalse(Q().Para(length=-2).s(d))

    def test_search_codeblock(self):
        d = self.parse(r"""
        ~~~ {#mycode .haskell .numberLines startFrom="100"}
        asdf
        ~~~
        """)
        self.assertTrue(Q().CodeBlock().s(d))
        self.assertTrue(Q().CodeBlock(hasID="mycode").s(d))
        self.assertFalse(Q().CodeBlock(hasID="notme").s(d))
        self.assertTrue(Q().CodeBlock(hasClass="haskell").s(d))
        self.assertFalse(Q().CodeBlock(hasClass="notme").s(d))
        self.assertTrue(Q().CodeBlock(text="asdf").s(d))
        self.assertFalse(Q().CodeBlock(text="notme").s(d))

    def test_search_rawblock(self):
        d = self.parse(r"""
        \begin{something}
        asdf
        \end{something}
        """)
        self.assertTrue(Q().RawBlock().s(d))
        self.assertTrue(Q().RawBlock(hasFormat="latex").s(d))
        self.assertFalse(Q().RawBlock(hasFormat="html").s(d))
        self.assertTrue(Q().RawBlock(
            text='\\begin{something}\nasdf\n\\end{something}').s(d))
        self.assertFalse(Q().RawBlock(text="notme").s(d))

    def test_search_blockquote(self):
        d = self.parse(r"""
        > asdf
        """)
        self.assertTrue(Q().BlockQuote().s(d))
        self.assertTrue(Q().BlockQuote(length=1).s(d))
        self.assertTrue(Q().BlockQuote(length=-1).s(d))
        self.assertFalse(Q().BlockQuote(length=2).s(d))
        self.assertFalse(Q().BlockQuote(length=-2).s(d))

    def test_search_orderedlist(self):
        d = self.parse(r"""
        1.  asdf
        2.  asdf

            asdf
        """)
        self.assertTrue(Q().OrderedList().s(d))
        self.assertTrue(Q().OrderedList(length=2).s(d))
        self.assertTrue(Q().OrderedList(length=-1).s(d))
        self.assertTrue(Q().OrderedList(length=-2).s(d))
        self.assertFalse(Q().OrderedList(length=3).s(d))
        self.assertFalse(Q().OrderedList(length=-3).s(d))
        self.assertTrue(Q().OrderedList(sublist_length=-1).s(d))
        self.assertFalse(Q().OrderedList(sublist_length=1).s(d))
        self.assertFalse(Q().OrderedList(sublist_length=2).s(d))
        self.assertFalse(Q().OrderedList(sublist_length=-2).s(d))

    def test_search_bulletlist(self):
        d = self.parse(r"""
        *   asdf
        *   asdf

            asdf
        """)
        self.assertTrue(Q().BulletList().s(d))
        self.assertTrue(Q().BulletList(length=2).s(d))
        self.assertTrue(Q().BulletList(length=-1).s(d))
        self.assertTrue(Q().BulletList(length=-2).s(d))
        self.assertFalse(Q().BulletList(length=3).s(d))
        self.assertFalse(Q().BulletList(length=-3).s(d))
        self.assertTrue(Q().BulletList(sublist_length=-1).s(d))
        self.assertFalse(Q().BulletList(sublist_length=1).s(d))
        self.assertFalse(Q().BulletList(sublist_length=2).s(d))
        self.assertFalse(Q().BulletList(sublist_length=-2).s(d))

    def test_search_header(self):
        d = self.parse(r"""
        ## asdf {#mycode .haskell .numberLines startFrom="100"}
        """)
        self.assertTrue(Q().Header().s(d))
        self.assertTrue(Q().Header(level=2).s(d))
        self.assertFalse(Q().Header(level=1).s(d))
        self.assertFalse(Q().Header(level=3).s(d))
        self.assertTrue(Q().Header(hasID="mycode").s(d))
        self.assertFalse(Q().Header(hasID="notme").s(d))
        self.assertTrue(Q().Header(hasClass="haskell").s(d))
        self.assertFalse(Q().Header(hasClass="notme").s(d))
        self.assertTrue(Q().Header(length=1).s(d))
        self.assertTrue(Q().Header(length=-1).s(d))
        self.assertFalse(Q().Header(length=2).s(d))
        self.assertFalse(Q().Header(length=-2).s(d))

    def test_search_horizontalrule(self):
        d = self.parse(r"""
        ----
        """)
        self.assertTrue(Q().HorizontalRule().s(d))

    def test_search_div(self):
        d = self.parse(r"""
        <div id="qwer" class="zxcv" uiop="hjkl">

        asdf

        </div>
        """)
        self.assertTrue(Q().Div().s(d))
        self.assertTrue(Q().Div(hasID="qwer").s(d))
        self.assertFalse(Q().Div(hasID="notme").s(d))
        self.assertTrue(Q().Div(hasClass="zxcv").s(d))
        self.assertFalse(Q().Div(hasClass="notme").s(d))
        self.assertTrue(Q().Div(length=1).s(d))
        self.assertTrue(Q().Div(length=-1).s(d))
        self.assertFalse(Q().Div(length=2).s(d))
        self.assertFalse(Q().Div(length=-2).s(d))

    def test_search_str(self):
        d = self.parse(r"""
        asdf
        """)
        self.assertTrue(Q().Str().s(d))
        self.assertTrue(Q().Str(text="asdf").s(d))
        self.assertFalse(Q().Str(text="notme").s(d))

    def test_search_emph(self):
        d = self.parse(r"""
        *asdf*
        """)
        self.assertTrue(Q().Emph().s(d))
        self.assertTrue(Q().Emph(length=1).s(d))
        self.assertTrue(Q().Emph(length=-1).s(d))
        self.assertFalse(Q().Emph(length=2).s(d))
        self.assertFalse(Q().Emph(length=-2).s(d))

    def test_search_strong(self):
        d = self.parse(r"""
        **asdf**
        """)
        self.assertTrue(Q().Strong().s(d))
        self.assertTrue(Q().Strong(length=1).s(d))
        self.assertTrue(Q().Strong(length=-1).s(d))
        self.assertFalse(Q().Strong(length=2).s(d))
        self.assertFalse(Q().Strong(length=-2).s(d))

    def test_search_strikeout(self):
        d = self.parse(r"""
        ~~asdf~~
        """)
        self.assertTrue(Q().Strikeout().s(d))
        self.assertTrue(Q().Strikeout(length=1).s(d))
        self.assertTrue(Q().Strikeout(length=-1).s(d))
        self.assertFalse(Q().Strikeout(length=2).s(d))
        self.assertFalse(Q().Strikeout(length=-2).s(d))

    def test_search_superscript(self):
        d = self.parse(r"""
        ^asdf^
        """)
        self.assertTrue(Q().Superscript().s(d))
        self.assertTrue(Q().Superscript(length=1).s(d))
        self.assertTrue(Q().Superscript(length=-1).s(d))
        self.assertFalse(Q().Superscript(length=2).s(d))
        self.assertFalse(Q().Superscript(length=-2).s(d))

    def test_search_subscript(self):
        d = self.parse(r"""
        ~asdf~
        """)
        self.assertTrue(Q().Subscript().s(d))
        self.assertTrue(Q().Subscript(length=1).s(d))
        self.assertTrue(Q().Subscript(length=-1).s(d))
        self.assertFalse(Q().Subscript(length=2).s(d))
        self.assertFalse(Q().Subscript(length=-2).s(d))

    def test_search_smallcaps(self):
        d = self.parse(r"""
        <span style="font-variant:small-caps;">asdf</span>
        """)
        self.assertTrue(Q().SmallCaps().s(d))
        self.assertTrue(Q().SmallCaps(length=1).s(d))
        self.assertTrue(Q().SmallCaps(length=-1).s(d))
        self.assertFalse(Q().SmallCaps(length=2).s(d))
        self.assertFalse(Q().SmallCaps(length=-2).s(d))

    def test_search_quoted(self):
        d = self.parse(r"""
        "asdf"
        """)
        self.assertTrue(Q().Quoted().s(d))
        self.assertTrue(Q().Quoted(length=1).s(d))
        self.assertTrue(Q().Quoted(length=-1).s(d))
        self.assertFalse(Q().Quoted(length=2).s(d))
        self.assertFalse(Q().Quoted(length=-2).s(d))

    def test_search_cite(self):
        d = self.parse(r"""
        [@asdf]
        """)
        self.assertTrue(Q().Cite().s(d))
        self.assertTrue(Q().Cite(length=1).s(d))
        self.assertTrue(Q().Cite(length=-1).s(d))
        self.assertFalse(Q().Cite(length=2).s(d))
        self.assertFalse(Q().Cite(length=-2).s(d))

    def test_search_code(self):
        d = self.parse(r"""
        `asdf`{#mycode .haskell .numberLines startFrom="100"}
        """)
        self.assertTrue(Q().Code().s(d))
        self.assertTrue(Q().Code(hasID="mycode").s(d))
        self.assertFalse(Q().Code(hasID="notme").s(d))
        self.assertTrue(Q().Code(hasClass="haskell").s(d))
        self.assertFalse(Q().Code(hasClass="notme").s(d))
        self.assertTrue(Q().Code(text="asdf").s(d))
        self.assertFalse(Q().Code(text="notme").s(d))

    def test_search_space(self):
        d = self.parse(r"""
        asdf asdf
        """)
        self.assertTrue(Q().Space().s(d))

    def test_search_linebreak(self):
        d = self.parse(r"""
        asdf \
        asdf
        """)
        self.assertTrue(Q().LineBreak().s(d))

    def test_search_math(self):
        d = self.parse(r"""
        $x$
        """)
        self.assertTrue(Q().Math().s(d))
        self.assertTrue(Q().Math(text="x").s(d))
        self.assertFalse(Q().Math(text="notme").s(d))

    def test_search_math(self):
        d = self.parse(r"""
        $x$
        """)
        self.assertTrue(Q().Math().s(d))
        self.assertTrue(Q().Math(text="x").s(d))
        self.assertFalse(Q().Math(text="notme").s(d))

    def test_search_rawinline(self):
        d = self.parse(r"""
        \textsf{asdf}
        """)
        self.assertTrue(Q().RawInline().s(d))
        self.assertTrue(Q().RawInline(hasFormat="tex").s(d))
        self.assertFalse(Q().RawInline(hasFormat="html").s(d))
        self.assertTrue(Q().RawInline(text=r'\textsf{asdf}').s(d))
        self.assertFalse(Q().RawInline(text="notme").s(d))

    def test_search_link(self):
        d = self.parse(r"""
        [asdf](http://asdf.com/)
        """)
        self.assertTrue(Q().Link().s(d))
        self.assertTrue(Q().Link(length=1).s(d))
        self.assertTrue(Q().Link(length=-1).s(d))
        self.assertFalse(Q().Link(length=2).s(d))
        self.assertFalse(Q().Link(length=-2).s(d))

    def test_search_image(self):
        d = self.parse(r"""
        ![asdf](http://asdf.com/)
        """)
        self.assertTrue(Q().Image().s(d))
        self.assertTrue(Q().Image(length=1).s(d))
        self.assertTrue(Q().Image(length=-1).s(d))
        self.assertFalse(Q().Image(length=2).s(d))
        self.assertFalse(Q().Image(length=-2).s(d))

    def test_search_note(self):
        d = self.parse(r"""
        asdf[^1]

        [^1]: qwer
        """)
        self.assertTrue(Q().Note().s(d))
        self.assertTrue(Q().Note(length=1).s(d))
        self.assertTrue(Q().Note(length=-1).s(d))
        self.assertFalse(Q().Note(length=2).s(d))
        self.assertFalse(Q().Note(length=-2).s(d))

    def test_search_span(self):
        d = self.parse(r"""
        <span id="qwer" class="zxcv" uiop="hjkl">asdf</span>
        """)
        self.assertTrue(Q().Span().s(d))
        self.assertTrue(Q().Span(hasID="qwer").s(d))
        self.assertFalse(Q().Span(hasID="notme").s(d))
        self.assertTrue(Q().Span(hasClass="zxcv").s(d))
        self.assertFalse(Q().Span(hasClass="notme").s(d))
        self.assertTrue(Q().Span(length=1).s(d))
        self.assertTrue(Q().Span(length=-1).s(d))
        self.assertFalse(Q().Span(length=2).s(d))
        self.assertFalse(Q().Span(length=-2).s(d))

    def test_search_nested(self):
        d = self.parse(r"""
        *   **asdf**
        *   *uiop*

            > **_qwer_ zxcv**
        """)
        self.assertTrue(Q().BulletList().s(d))
        self.assertTrue(Q().BlockQuote().s(d))
        self.assertTrue(Q().Para().s(d))
        self.assertTrue(Q().Strong().s(d))
        self.assertTrue(Q().Emph().s(d))
        self.assertTrue(Q().Str(text="qwer").s(d))

    def test_search_multiple(self):
        d = self.parse(r"""
        *asdf*

        *   hjkl *qwer* fgh[^1]

        > *zxcv*
        >
        > 1.  bnm^*uiop*^

        [^1]: [*aoeu*](http://dhtns.com/)
        """)
        m = Q().Emph().search(d)
        self.assertEqual(len(m.group(0)), 5)
        s = [Q().Emph()[0].Str().match(x).group(0)['c'] for x in m.group(0)]
        self.assertEqual(set(s), set(['asdf', 'qwer', 'zxcv', 'uiop', 'aoeu']))

        m2 = Q().Emph().search(d, count = 3)
        self.assertEqual(len(m2.group(0)), 3)
        s = [Q().Emph()[0].Str().match(x).group(0)['c'] for x in m2.group(0)]
        self.assertTrue(set(s) < set(['asdf', 'qwer', 'zxcv', 'uiop', 'aoeu']))


if __name__ == '__main__':
    unittest.main()
